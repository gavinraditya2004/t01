package com.company;

class Main {
    public static class Mahasiswa {
        String nama; int nim;
        String prodi; int tahunmasuk;
        Transkrip[] transkrip;
    }
    public static class Kursus {
        String nama;
        int kodematkul;
        int sks;
    }

    public static class Transkrip {
        Mahasiswa mhs;
        Kursus kursus;
        int nilai;
    }
    public static void addTranscript(Mahasiswa mahasiswa, Transkrip transkrip, Transkrip[] transkrips) {
        Transkrip[] transkripBaru = new Transkrip[transkrips.length + 1];
        for (int i = 0; i < transkrips.length; i++) {
            transkripBaru[i] = transkrips[i];
        }
        transkripBaru[transkrips.length] = transkrip;
        mahasiswa.transkrip = transkripBaru;
    }
    public static void title(Mahasiswa mhs, Transkrip[] transkrips) {
        System.out.println("Transkrip nilai untuk " + mhs.nama + ":");
        for (Transkrip transcript : transkrips) {
            if (transcript != null && transcript.mhs == mhs) {
                cetakTranscript(transcript);
            }
        }
    }
    public static void cetakTranscript(Transkrip transcript) {
        System.out.println(transcript.kursus.nama + ": " + transcript.nilai);
    }

    public static void main(String[] args) {
        Mahasiswa mhs1 = new Mahasiswa();
        mhs1.nama= "gavin raditya";
        mhs1.nim=334413;
        mhs1.prodi="Teknologi Informasi";
        mhs1.tahunmasuk=2022;
        //LENGKAPI CODE DISINI (5)

        Mahasiswa mhs2 = new Mahasiswa();
        mhs2.nama= "diyyas ilyasa";
        mhs2.nim=3232323;
        mhs2.prodi="Teknik Informatika";
        mhs2.tahunmasuk=2024;
        //LENGKAPI CODE DISINI (2.5)

        Kursus mk1 = new Kursus();
        mk1.nama="ddap";
        mk1.sks=3;
        mk1.kodematkul=1333;
        //LENGKAPI CODE DISINI (5)

        Kursus mk2 = new Kursus();
        mk2.nama = "sbd";
        mk2.sks=4;
        mk2.kodematkul=2434;
        //LENGKAPI CODE DISINI (2.5)

        Transkrip[] transcripts = new Transkrip[4];

        Transkrip t1 = new Transkrip();
        t1.mhs= mhs1;
        t1.kursus= mk1;
        t1.nilai= 90;
        transcripts[0]=t1;
        //LENGKAPI CODE DISINI (7.5)

        Transkrip t2 = new Transkrip();
        t2.mhs = mhs1;
        t2.kursus = mk2;
        t2.nilai = 86;
        transcripts[1]=t2;
        //LENGKAPI CODE DISINI (2.5)

        Transkrip t3 = new Transkrip();
        t3.mhs=mhs2;
        t3.kursus=mk1;
        t3.nilai=80;
        transcripts[2]=t3;
        //LENGKAPI CODE DISINI (2.5)

        Transkrip t4 = new Transkrip();
        t4.mhs=mhs2;
        t4.kursus=mk2;
        t4.nilai=75;
        transcripts[3]=t4;
        //LENGKAPI CODE DISINI (2.5)

        addTranscript(mhs1, t1, transcripts);
        addTranscript(mhs1, t2, transcripts);
        title(mhs1, transcripts);
        addTranscript(mhs2, t3, transcripts);
        addTranscript(mhs2, t4, transcripts);
        title(mhs2,transcripts);
}
}

